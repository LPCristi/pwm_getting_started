`include "macros_ord.v"

module pwmGenerator(
input clk, 
input rst, 
input enable, 
input cmdVal, 
input cmdRW, 
input [1:0] addr, 
input [15:0] dataIn, 
output reg [15:0] dataOut,  
output wire PwmOut);


	reg [15:0] Fact_Umpl;
	reg [15:0] Per; 
	reg [2:0] Presc;
	reg Contr;

	reg [15:0] Fact_Umpl_Old;
	reg [15:0] Per_Old; 
	reg [2:0] Presc_Old;
	reg Contr_Old;

	reg [15:0] counter;
	wire clk_new;
	reg [15:0] counter_PWM;
	reg VarUpdate;
	wire PwmNew;


	assign PwmNew = counter_PWM < Fact_Umpl_Old ? 1:0;
	assign PwmOut = (Fact_Umpl_Old > Per_Old) ? 1 : (enable ? PwmNew : 1'bZ);
	assign clk_new = (Presc_Old == 0) ? clk : ~counter[Presc_Old];
	
	always @(posedge clk)
	begin
		if(rst == 1'b1)
		begin
			Fact_Umpl <= 16'h0000;
			Per <= 16'h0000;
			Presc <= 3'b000;
			Contr <= 1'b0;


			Fact_Umpl_Old <= 16'h0000;
			Per_Old <= 16'h0000;
			Presc_Old <= 3'b000;
			Contr_Old <= 1'b0;

			counter_PWM <= 0;

			counter <= 0;
			VarUpdate <= 0;
			
		end
	
	end
		

	always@(posedge clk)
	begin

		if(rst == 1'b0)
		begin


			if(cmdVal == 1'b1)
			begin

				//scriere registri
				if(cmdRW == 1'b0)//operatie scriere
				begin


					case(addr)
						`FACTOR_UMPLERE: Fact_Umpl <= dataIn;
						`PERIOADA: Per <= dataIn;
						`PRESCALER: Presc <= dataIn[2:0];
						`CONTROL: Contr <= dataIn[0];
					endcase
					VarUpdate = 1;
					
				end
				//citire registri
				if(cmdRW == 1'b1)//operatie citire
				begin
					case(addr)
						`FACTOR_UMPLERE: dataOut <= Fact_Umpl;
						`PERIOADA: dataOut <= Per;
						`PRESCALER: dataOut <= Presc;
						`CONTROL: dataOut <= Contr;
					endcase
				end


			end 

		end
	end 

	//counter generare presc
	always @(posedge clk)
	begin


		if(Presc_Old == 0)
		begin 
			counter <= 0;
		end else begin
			counter <= counter + 1;
		end
		
	end


	
	always @(posedge clk_new)
	begin
		if(    (Per_Old!=0) 
		    && (counter_PWM < Per_Old -1) 
		   )
		begin
			counter_PWM <= counter_PWM + 1;
		end else begin
			counter_PWM <= 0;
			if(VarUpdate == 1)
			begin
				Fact_Umpl_Old <= Fact_Umpl;
				Per_Old <= Per;
				Presc_Old <= Presc;
				Contr_Old <= Contr;
				VarUpdate = 0;
					
				end
			end
	end


endmodule
