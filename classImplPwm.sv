`ifndef IMPLPWMCLASS
`define IMPLPWMCLASS

`include "structuri.sv"
`include "classIMPL.sv"

class impl_pwm #(type T) extends impl_intf #(Pck_Out);
  T sb;
  function new (T sb);
    this.sb = sb;
  endfunction
  function write(Pck_Out pack); 
	sb.write_out(pack);
  endfunction

endclass


`endif