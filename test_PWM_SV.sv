`include "macros_ord.v"
`include "classMonitorIn.sv"
`include "classMonitorOut.sv"
`include "classSB.sv"

module test_PWM();

	reg clk;
	reg rst;
	reg enable;
	reg cmdVal;
	reg cmdRW;
	reg [1:0] addr;
	reg [15:0] dataIn;
	wire [15:0] dataOut;
	wire PwmOut;

	pwmGenerator gen (.clk(clk), 
			  .rst(rst), 
			  .enable(enable), 
			  .cmdVal(cmdVal), 
			  .cmdRW(cmdRW),
 			  .addr(addr), 
			  .dataIn(dataIn),  
			  .dataOut(dataOut),  
			  .PwmOut(PwmOut) 
			  );
	
  	Interfata_intrare DateIntrare(.clk(clk), 
		   		.rst(rst), 
		    		.enable(enable), 
		   		.cmdVal(cmdVal), 
		   		.cmdRW(cmdRW), 
		   		.addr(addr), 
		    		.dataIn(dataIn), 
		    		.dataOut(dataOut)
		    		);

  	Interfata_iesire DateIesire(.PwmOut(PwmOut));
	
	MonitorIN monIn;
	MonitorOut monOut;
	ScoreBoard SB1;

   task writeReg(input [15:0] ad, input [15:0] da);
 	begin
	addr=ad;
	dataIn=da;
	cmdVal=1'b1;
	cmdRW = 1'b0;
	
	@ (posedge clk);

	addr= 16'bx;
	dataIn=16'bx;
	cmdVal=1'b0;	
	cmdRW = 1'bx;
	end
   endtask

   task readReg(input [15:0] ad);
 	begin
	addr=ad;
	cmdVal=1'b1;
	cmdRW = 1'b1;
	#10
	@ (posedge clk);

	addr= 16'bx;
	cmdVal=1'b0;	
	cmdRW = 1'bx;
	end
   endtask


	initial 
	begin
	
		SB1 = new(1);

		monIn = new(DateIntrare);
		monOut = new(DateIesire);
		
		monIn.port_r.connect(SB1.ImplR);
		monIn.port_e.connect(SB1.ImplE);
		monIn.port_tr.connect(SB1.ImplT);
		monOut.port_pwm.connect(SB1.ImplPwm);
		
		fork
			monIn.checkReset();
			monIn.checkEnable();
			monIn.checkCmdVal();
			monIn.checkCmdRW();
			monIn.checkScriereSauCitire();
			monOut.FUandPer();
			
		join
	end



	initial 
	begin
		clk=1'b1;
		
		forever #5
		begin 
			clk = ~clk;
		end
	end

	initial 
	begin
		rst = 1'b0;

		#10
		rst = 1'b1;

		#20 
		rst = 1'b0;
		enable = 1'b0;

		#10
		enable = 1'b1;

//scriere date registri

		
		#101
		writeReg(`FACTOR_UMPLERE, 16'h0005);
		writeReg(`PERIOADA, 16'h000B);
		writeReg(`PRESCALER,16'h0004);
		writeReg(`CONTROL, 16'h0001);

		#11
		readReg(`FACTOR_UMPLERE);
		readReg(`PERIOADA);
		readReg(`PRESCALER);
		readReg(`CONTROL);

		#1003
		writeReg(`FACTOR_UMPLERE, 16'h0007);
		writeReg(`PERIOADA, 16'h000A);
		writeReg(`PRESCALER,16'h0004);
		writeReg(`CONTROL, 16'h0001);

//citire date registri	

		#14
		readReg(`PRESCALER);
		readReg(`PERIOADA);
		readReg(`CONTROL);
		readReg(`FACTOR_UMPLERE);

//modificare contr

		#11
		writeReg(`CONTROL, 16'h0000);
		
//modificare valori --> factor umplere > perioada
		#14
		writeReg(`FACTOR_UMPLERE, 16'h0009);
		writeReg(`PERIOADA, 16'h0004);
		writeReg(`CONTROL, 16'h0001);

//modificare valori
		#1004
		writeReg(`FACTOR_UMPLERE, 16'h0002);
		writeReg(`PERIOADA, 16'h0004);
		writeReg(`CONTROL, 16'h0001);
		
//modificare valori --> contr = 0
		#100001
		writeReg(`FACTOR_UMPLERE, 16'h0002);
		writeReg(`PERIOADA, 16'h0005);
		writeReg(`PRESCALER,16'h0001);
		writeReg(`CONTROL, 16'h0000);
		
		#30006
		writeReg(`PRESCALER,16'h0007);
	
		#10001
		rst =1'b1;
		
		#10002
		rst =1'b0;

		#11
		writeReg(`FACTOR_UMPLERE, 16'h0002);
		writeReg(`PERIOADA, 16'h0005);
		writeReg(`PRESCALER,16'h0001);
		writeReg(`CONTROL, 16'h0001); 
		

	end

	initial
	begin
		#300000 $finish();
	end


endmodule