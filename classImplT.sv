`ifndef IMPLTCLASS
`define IMPLTCLASS

`include "structuri.sv"
`include "classIMPL.sv"
//`include "classSB.sv"

class impl_t #(type T) extends impl_intf #(Pck_Tranz);
  T sb;
  //Pck_RST pr;
  function new (T sb);
    this.sb = sb;
  endfunction
  function write(Pck_Tranz pack); 
	sb.write_in_tranz(pack);
  endfunction

endclass


`endif