`ifndef CLASSMONIN
`define CLASSMONIN

`include "classPORT.sv"
`include "structuri.sv"

 class MonitorIN;

 virtual interface Interfata_intrare intIn;

 Pck_In In_pack;
 Pck_RST RST_pack;
 Pck_EN EN_pack;
 Pck_Tranz Tranz_pack;
 
 port #(Pck_RST) port_r;
 port #(Pck_EN) port_e;
 port #(Pck_Tranz) port_tr;
 
 function new (virtual Interfata_intrare intIn);
   this.intIn = intIn;
   port_r = new();
   port_e = new();
   port_tr = new();
 endfunction


 task checkReset;
   integer current_time;
   forever begin
    @ (posedge intIn.clk)
    begin
     @ (posedge intIn.rst) begin
      current_time = $time;
      $display("Resetul a trecut in 1 la momentul de timp: %0d", current_time);
	  RST_pack.rst_s = intIn.rst;
	  port_r.write(RST_pack);
     end
     @ (negedge intIn.rst) begin
      current_time = $time;
      $display("Resetul a trecut in 0 la momentul de timp: %0d", current_time);
	  RST_pack.rst_s = intIn.rst;
	  port_r.write(RST_pack);
     end
    end
   end
 endtask



 task checkEnable;
   integer current_time;
   forever begin
   @ (intIn.enable)
   begin
    if(intIn.enable == 1) begin
     current_time = $time;
     $display("enable a trecut in 1 la momentul de timp: %0d", current_time);
	 EN_pack.enable_s = intIn.enable;
	 port_e.write(EN_pack);
    end
    if(intIn.enable == 0) begin
     current_time = $time;
     $display("enable a trecut in 0 la momentul de timp: %0d", current_time);
	 EN_pack.enable_s = intIn.enable;
	 port_e.write(EN_pack);
    end
   end
   end
 endtask

task checkCmdVal;
   integer current_time;
   forever begin
   @ (posedge intIn.clk)
   begin
    @ (intIn.cmdVal) begin
     if(intIn.cmdVal == 1) begin
      current_time = $time;
      $display("cmdVal a trecut in 1 la momentul de timp: %0d", current_time);
     end
     if(intIn.cmdVal == 0) begin
      current_time = $time;
      $display("cmdVal a trecut in 0 la momentul de timp: %0d", current_time);
     end
    end
   end
  end
 endtask

task checkCmdRW;
   integer current_time;
   forever begin
   @ (posedge intIn.clk)
   begin
    @ (intIn.cmdRW) begin
     if(intIn.cmdRW==1) begin
       current_time = $time;
       $display("cmdRW a trecut in 1 la momentul de timp: %0d", current_time);
     end
     if(intIn.cmdRW==0) begin
       current_time = $time;
       $display("cmdRW a trecut in 0 la momentul de timp: %0d", current_time);
     end
    end
   end
  end
 endtask


task checkScriereSauCitire;
   integer current_time;
   forever begin
   @ (posedge intIn.clk)
   begin
    if (intIn.cmdVal == 1) begin
     
      if(intIn.cmdRW==1) begin
        current_time = $time;
	  
	  Tranz_pack.cmdRW_s = intIn.cmdRW;
	  Tranz_pack.addr_s = intIn.addr;
	  @ (posedge intIn.clk) begin 
		Tranz_pack.dataOut_s =intIn.dataOut;
		port_tr.write(Tranz_pack);
	  end
          $display("La momentul de timp %0d a avut loc o citire de la adresa %0d. S-a citit valoarea %0d", $time, In_pack.addr_s, intIn.dataOut);
      end
      if(intIn.cmdRW==0) begin
        current_time = $time;
        $display("La momentul de timp %0d a avut loc o scriere la adresa %0d. S-a scris valoarea %0d", $time, intIn.addr, intIn.dataIn);
	  
	  Tranz_pack.cmdRW_s = intIn.cmdRW;
	  Tranz_pack.addr_s = intIn.addr;
	  Tranz_pack.dataIn_s = intIn.dataIn;
	  
	  port_tr.write(Tranz_pack);
      end
    end
   end
  end
 endtask
  

endclass

`endif
