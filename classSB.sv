`ifndef CLASSSB
`define CLASSSB

`include "structuri.sv"
`include "macros_ord.v"

`include "classImplR.sv" 
`include "classImplE.sv" 
`include "classImplT.sv" 
`include "classImplPwm.sv" 
`include "implDec.sv"

`DECL_IMPL(in_rst)
`DECL_IMPL(in_en)
`DECL_IMPL(in_tranz)
`DECL_IMPL(out)


`define clkPer 10

 class ScoreBoard;
	real SB_number;
	impl_in_rst #(ScoreBoard, Pck_RST) ImplR;
	impl_in_en #(ScoreBoard, Pck_EN) ImplE;
	impl_in_tranz #(ScoreBoard, Pck_Tranz) ImplT;
	impl_out #(ScoreBoard, Pck_Out) ImplPwm;
  function new (real sb_num);
 	SB_number = sb_num;
	ImplR = new(this);
	ImplE = new(this);
	ImplT = new(this);
	ImplPwm = new(this);
	$display("THIS IS SCOREBOARD No. %0d constructor", sb_num);
  endfunction



    bit rst_sb;
	bit enable_sb;
	bit cmdRW_sb;
	bit [1:0] addr_sb;
	bit [15:0] dataIn_sb; 
	bit [15:0] dataOut_sb;
	
	logic PWM_Out_sb;
    real ceAmScrisInPer, ceAmScrisInFU, ceAmScrisInPresc, ceAmScrisInContr;
    real ceAmScrisInPerBACK, ceAmScrisInFUBACK, ceAmScrisInPrescBACK, ceAmScrisInContrBACK;

	real noOfPosedge = 0;

	bit notOk;
	real start_time = 0;
    real poz_time = 0;
    real finish_time = 0; 
    real fact_umpl = 0;
    real perioada = 0; 
    bit ok;
	real fact_umpl_IN, perioada_IN;

 task write_in(Pck_In inPck);
   
   rst_sb = inPck.rst_s;
   enable_sb = inPck.enable_s;
   cmdRW_sb = inPck.cmdRW_s;
   addr_sb = inPck.addr_s;
   dataIn_sb = inPck.dataIn_s;
   dataOut_sb = inPck.dataOut_s;

   if(rst_sb == 1) begin
	   $display("La momentul t= %0d am primit rst = %0d", $time, rst_sb);
   end 
   if(rst_sb == 0) begin
	   $display("La momentul t= %0d am primit rst = %0d", $time, rst_sb);
   end 
   
   
   if(enable_sb == 1) begin
	   $display("La momentul t= %0d am primit enable = %0d", $time, enable_sb);
   end 
   if(enable_sb == 0) begin
	   $display("La momentul t= %0d am primit enable = %0d", $time, enable_sb);
   end 

   if(cmdRW_sb == 0) begin 
     $display("!!!!!!!!!!!!!!!!!!!!!! t= %0d ---> cmdRW = %0d; adr = %0d; dIn = %0d", $time, cmdRW_sb, addr_sb, dataIn_sb);
	case(addr_sb)
	 `FACTOR_UMPLERE: ceAmScrisInFU <= dataIn_sb;
	 `PERIOADA: ceAmScrisInPer <= dataIn_sb;
	 `PRESCALER: ceAmScrisInPresc <= dataIn_sb;
	 `CONTROL: ceAmScrisInContr <= dataIn_sb;
	endcase
   end else begin
      if(cmdRW_sb == 1) begin 
      $display("!!!!!!!!!!!!!!!!!!!!!! t= %0d ---> cmdRW = %0d; adr = %0d; dOut = %0d", $time, cmdRW_sb, addr_sb, dataOut_sb);
	 case(addr_sb)
	  `FACTOR_UMPLERE: begin notOk = ceAmScrisInFU == dataOut_sb ? 0: 1; 
   			         if(notOk == 1) 
  				 begin 
   				     $display("Eroare =======> Ce s-a scris nu corespunde cu ce s-a citit FU"); 
				 end else begin 
				     $display("GOOD =======> Citire reusita a factorului de umplere"); 
				 end
			   end
	  `PERIOADA: begin notOk = ceAmScrisInPer == dataOut_sb ? 0: 1; 
   			         if(notOk == 1) 
  				 begin 
   				     $display("Eroare =======> Ce s-a scris nu corespunde cu ce s-a citit PERIOADA"); 
				 end else begin 
				     $display("GOOD =======> Citire reusita a perioadei"); 
				 end
			   end
	  `PRESCALER: begin notOk = ceAmScrisInPresc == dataOut_sb ? 0: 1; 
   			         if(notOk == 1) 
  				 begin 
   				     $display("Eroare =======> Ce s-a scris nu corespunde cu ce s-a citit PRESCALER"); 
				 end else begin 
				     $display("GOOD =======> Citire reusita a prescalerului"); 
				 end
			   end
	  `CONTROL: begin notOk = ceAmScrisInContr == dataOut_sb ? 0: 1;
   			         if(notOk == 1) 
  				 begin 
   				     $display("Eroare =======> Ce s-a scris nu corespunde cu ce s-a citit CONTROL"); 
				 end else begin 
				     $display("GOOD =======> Citire reusita a controlului"); 
				 end
		   end
	 endcase
      end
   end
 endtask
 
 task write_in_tranz(Pck_Tranz tPack);

   cmdRW_sb = tPack.cmdRW_s;
   addr_sb = tPack.addr_s;
   dataIn_sb = tPack.dataIn_s;
   dataOut_sb = tPack.dataOut_s;

   if(cmdRW_sb == 0) begin 
     $display("!!!!!!!!!!!!!!!!!!!!!! t= %0d ---> cmdRW = %0d; adr = %0d; dIn = %0d", $time, cmdRW_sb, addr_sb, dataIn_sb);
	case(addr_sb)
	 `FACTOR_UMPLERE: ceAmScrisInFU <= dataIn_sb;
	 `PERIOADA: ceAmScrisInPer <= dataIn_sb;
	 `PRESCALER: ceAmScrisInPresc <= dataIn_sb;
	 `CONTROL: ceAmScrisInContr <= dataIn_sb;
	endcase
   end else begin
      if(cmdRW_sb == 1) begin 
      $display("!!!!!!!!!!!!!!!!!!!!!! t= %0d ---> cmdRW = %0d; adr = %0d; dOut = %0d", $time, cmdRW_sb, addr_sb, dataOut_sb);
	 case(addr_sb)
	  `FACTOR_UMPLERE: begin notOk = ceAmScrisInFU == dataOut_sb ? 0: 1; 
   			         if(notOk == 1) 
  				 begin 
   				     $display("Eroare =======> Ce s-a scris nu corespunde cu ce s-a citit FU"); 
				 end else begin 
				     $display("GOOD =======> Citire reusita a factorului de umplere"); 
				 end
			   end
	  `PERIOADA: begin notOk = ceAmScrisInPer == dataOut_sb ? 0: 1; 
   			         if(notOk == 1) 
  				 begin 
   				     $display("Eroare =======> Ce s-a scris nu corespunde cu ce s-a citit PERIOADA"); 
				 end else begin 
				     $display("GOOD =======> Citire reusita a perioadei"); 
				 end
			   end
	  `PRESCALER: begin notOk = ceAmScrisInPresc == dataOut_sb ? 0: 1; 
   			         if(notOk == 1) 
  				 begin 
   				     $display("Eroare =======> Ce s-a scris nu corespunde cu ce s-a citit PRESCALER"); 
				 end else begin 
				     $display("GOOD =======> Citire reusita a prescalerului"); 
				 end
			   end
	  `CONTROL: begin notOk = ceAmScrisInContr == dataOut_sb ? 0: 1;
   			         if(notOk == 1) 
  				 begin 
   				     $display("Eroare =======> Ce s-a scris nu corespunde cu ce s-a citit CONTROL"); 
				 end else begin 
				     $display("GOOD =======> Citire reusita a controlului"); 
				 end
		   end
	 endcase
      end
   end
 endtask
 
 
task write_in_rst(Pck_RST rPck);
   rst_sb = rPck.rst_s;

   if(rst_sb == 1) begin
	   $display("La momentul t= %0d am primit rst = %0d", $time, rst_sb);
   end 
   if(rst_sb == 0) begin
	   $display("La momentul t= %0d am primit rst = %0d", $time, rst_sb);
   end 
endtask

task write_in_en(Pck_EN ePck);
   enable_sb = ePck.enable_s; 

   if(enable_sb == 1) begin
	   $display("La momentul t= %0d am primit enable = %0d", $time, enable_sb);
   end 
   if(enable_sb == 0) begin
	   $display("La momentul t= %0d am primit enable = %0d", $time, enable_sb);
   end 
endtask

 task write_out(Pck_Out outPck);
   PWM_Out_sb = outPck.PWM_Out_s;
   fact_umpl = 0;
   perioada = 0; 
     if(PWM_Out_sb === 'z) begin
	if(enable_sb == 1) begin 
 	  $display("->>>> Enable = 1; Perioada = z");
	end
     end

     if(PWM_Out_sb == 1)
     begin
       noOfPosedge = noOfPosedge +1;
       if(noOfPosedge == 2)
	begin
	  ceAmScrisInPrescBACK = ceAmScrisInPresc;
 	  ceAmScrisInPerBACK = ceAmScrisInPer;
	  ceAmScrisInFUBACK = ceAmScrisInFU;
	  ceAmScrisInContrBACK = ceAmScrisInContr;
	  noOfPosedge = 0;
	end
	
	$display("%0d =ceAmScrisInFUBACK: %0d; ceAmScrisInPerBACK: %0d; ceAmScrisInPrescBACK: %0d; ceAmScrisInContrBACK: : %0d ||||| =ceAmScrisInFU: %0d; ceAmScrisInPer: %0d; ceAmScrisInPresc: %0d; ceAmScrisInContr: : %0d", $time, ceAmScrisInFUBACK, ceAmScrisInPerBACK, ceAmScrisInPrescBACK, ceAmScrisInContrBACK, ceAmScrisInFU, ceAmScrisInPer, ceAmScrisInPresc, ceAmScrisInContr);
	if(ok==0) begin
	start_time=$time;
	ok = 1;
       end
       else begin
	finish_time = $time;
	perioada = finish_time - start_time;
	if(poz_time != 0)
	begin
	 fact_umpl = poz_time - start_time;
	//$display("======>>>> ceAmScrisInPrescBACK: %0d, ceAmScrisInPerBACK: %0d", ceAmScrisInPrescBACK, ceAmScrisInPerBACK);
  	perioada_IN = `clkPer * (1 << (ceAmScrisInPrescBACK + 1)) * ceAmScrisInPerBACK;
	
	if(perioada == perioada_IN)
	 begin
		$display("Perioada este corecta");
	 end else begin
		$display("Perioada gresita! Expected: %0d; Reality: %0d", perioada_IN, perioada);
	end
	if(fact_umpl == fact_umpl_IN)
	 begin
		$display("Factorul de umplere este corect");
	   	
	 end else begin
		$display("Factor de umplere gresit! Expected: %0d; Reality: %0d", fact_umpl_IN, fact_umpl);
	 end
	 
	 start_time = finish_time;
	 poz_time = 0;
	 finish_time = 0;
	
	end
       end
     end
     if(PWM_Out_sb == 0)
     begin
	poz_time = $time;
	fact_umpl_IN = `clkPer * (1 << (ceAmScrisInPrescBACK +1)) * ceAmScrisInFUBACK;
     end

 endtask

endclass

`endif
