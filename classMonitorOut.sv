`ifndef CLASSMONOUT
`define CLASSMONOUT

`include "classPORT.sv"
`include "structuri.sv"

class MonitorOut;

 virtual interface Interfata_iesire intOut;
 Pck_Out Out_pack;
 port #(Pck_Out) port_pwm;
 
 function new (virtual Interfata_iesire intOut);
   this.intOut = intOut;
   port_pwm = new();
 endfunction

 task FUandPer;
   integer start_time = 0;
   integer poz_time = 0;
   integer finish_time = 0; 
   real fact_umpl = 0;
   real perioada = 0; 

   real fact_umpl_old = 0;
   real perioada_old = 0; 

   real fact_umpl_nsec = 0;


   int ok;
   forever begin

	 if(intOut.PwmOut===1'bX) begin Out_pack.PWM_Out_s = intOut.PwmOut; port_pwm.write(Out_pack); end
     if(intOut.PwmOut===1'bZ) begin Out_pack.PWM_Out_s = intOut.PwmOut; port_pwm.write(Out_pack); end


     @ (posedge intOut.PwmOut)
     begin
	   Out_pack.PWM_Out_s = intOut.PwmOut;
	   port_pwm.write(Out_pack);
       if(ok==0) begin
	    start_time=$time;
	    ok = 1;
       end
       else begin
	finish_time = $time;
	perioada = finish_time - start_time;
	if(poz_time != 0)
	begin
	 fact_umpl_nsec = poz_time - start_time;
	 fact_umpl = (fact_umpl_nsec/perioada) * 100;

	 if(fact_umpl_old != fact_umpl)
	 begin
		$display("S-a schimbat factorul de umplere din %0d (old) in %0d (new) la momentul %0d", fact_umpl_old, fact_umpl, start_time);
	 end
	 if(perioada_old != perioada)
	 begin
		$display("S-a schimbat perioada din %0d (old) in %0d (new) la momentul %0d", perioada_old, perioada, start_time);
	 end
	 
	 start_time = finish_time;
	 poz_time = 0;
	 finish_time = 0;
	 
	fact_umpl_old = fact_umpl;
	perioada_old = perioada;

	end
       end
     end
     @ (negedge intOut.PwmOut)
	begin
	 Out_pack.PWM_Out_s = intOut.PwmOut;
	 port_pwm.write(Out_pack);
	 poz_time = $time;
	end
     end
 endtask
endclass


`endif