`ifndef IMPLRCLASS
`define IMPLRCLASS

`include "structuri.sv"
`include "classIMPL.sv"


class impl_r #(type T) extends impl_intf #(Pck_RST);
  T sb;

  function new (T sb);
    this.sb = sb;
  endfunction
  function write(Pck_RST pack); 
	sb.write_in_rst(pack);
  endfunction

endclass


`endif