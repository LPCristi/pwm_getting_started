`ifndef IMPLGEN
`define IMPLGEN

`include "structuri.sv"
`include "classIMPL.sv"

`define DECL_IMPL(X) \
class impl_``X #(type T, type P) extends impl_intf #(P); \
  T sb; \
  function new (T sb); \
    this.sb = sb; \
  endfunction \
  function write(P pack); \
	sb.write_``X(pack); \
  endfunction \
endclass 



`endif