`ifndef INTERFETE
`define INTERFETE

interface Interfata_intrare (
 input clk,
 input rst,
 input enable,
 input cmdVal,
 input cmdRW,
 input [1:0] addr, 
 input [15:0] dataIn,
 output [15:0] dataOut

);

endinterface

interface Interfata_iesire(
 output PwmOut
);
endinterface


`endif