`ifndef CLASSPORT
`define CLASSPORT

`include "classIMPL.sv"

class port #(type T);
  
  impl_intf #(T) implQ[$];
  function write(T pack);
   foreach(implQ[i]) begin

     implQ[i].write(pack);
   end
  endfunction
  
  
  function connect(impl_intf #(T) implI);
	implQ.push_back(implI);
  endfunction

endclass


`endif