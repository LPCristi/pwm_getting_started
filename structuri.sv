`ifndef STRUCTURI
`define STRUCTURI

typedef struct packed {
	bit rst_s;
	bit enable_s;
	bit cmdRW_s;
	bit [1:0] addr_s;
	bit [15:0] dataIn_s;
	bit [15:0] dataOut_s;
} Pck_In;


typedef struct packed {
	logic PWM_Out_s;
} Pck_Out;

typedef struct packed {
	bit rst_s;
} Pck_RST;

typedef struct packed {
	bit enable_s;
} Pck_EN;

typedef struct packed {
	bit cmdRW_s;
	bit [1:0] addr_s;
	bit [15:0] dataIn_s;
	bit [15:0] dataOut_s;
} Pck_Tranz;

`endif

