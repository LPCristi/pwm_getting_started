`ifndef IMPLECLASS
`define IMPLECLASS

`include "structuri.sv"
`include "classIMPL.sv"

class impl_e #(type T) extends impl_intf #(Pck_EN);
  T sb;
  function new (T sb);
    this.sb = sb;
  endfunction
  function write(Pck_EN pack); 
	sb.write_in_en(pack);
  endfunction

endclass


`endif